<?php

/**
 * @file
 * Contains forms and pages for instagram admin pages.
 */

/**
 * Form for authenticating user with Instagram API.
 */
function domain_instagram_admin_settings($form, &$form_state) {
  global $_domain;
  $domain_id = $_domain['domain_id'];
  $form = array();

  // Create a keyed array of blank defaults for the storage variable.
  $empty = array(
    'access_token' => '',
  );

  // Store data from variable in $form for now.
  $form['#data'] = variable_get("domain_instagram_admin_settings_{$domain_id}", $empty);

  // This is the field fieldset and group.
  $form['instagram_group'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['instagram_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Instagram configuration'),
    '#collapsible' => TRUE,
    '#description' => t('Domain Instagram requires connecting to a specific Instagram account. You need to be able to log into that account when asked to. The <a href="!help">Authenticate with Instagram</a> page helps with the setup.', array('!help' => 'https://www.drupal.org/node/2746185')),
    '#group' => 'instagram_group'
  );
  $form['instagram_fieldset']['access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Token'),
    '#description' => t('Your Instagram access token. Eg. 460786509.ab103e5.a54b6834494643588d4217ee986384a8'),
    '#default_value' => isset($form['#data']['access_token']) ? $form['#data']['access_token'] : '',
  );
  
  // This is the advance field fieldset and group.
  $form['instagram_advance_group'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['instagram_advance_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Instagram advance configuration'),
    '#collapsible' => TRUE,
    '#description' => t('Edit number of image, image width, image height and image resolution'),
    '#group' => 'instagram_advance_group'
  );
  $form['instagram_advance_fieldset']['count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of images to display.'),
    '#default_value' => isset($form['#data']['count']) ? $form['#data']['count'] : '4',
  );
  $form['instagram_advance_fieldset']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Image width in pixels.'),
    '#default_value' => isset($form['#data']['width']) ? $form['#data']['width'] : '100',
  );
  $form['instagram_advance_fieldset']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Image height in pixels.'),
    '#default_value' => isset($form['#data']['height']) ? $form['#data']['height'] : '100',
  );
  $image_options = array(
    'thumbnail' => t('Thumbnail Preview'),
    'low_resolution' => t('Low Resolution'),
    'standard_resolution' => t('Standard Resolution'),
    '#default_value' => isset($form['#data']['access_token']) ? $form['#data']['access_token'] : '',
  );
  $form['instagram_advance_fieldset']['img_resolution'] = array(
    '#type' => 'select',
    '#title' => t('Image resolution'),
    '#description' => t('Choose the quality of the images you would like to display.'),
    '#options' => $image_options,
    '#default_value' => isset($form['#data']['img_resolution']) ? $form['#data']['img_resolution'] : 'thumbnail',
  );
  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#validate'][] = 'domain_instagram_admin_settings_validate';
  $form['#submit'][] = 'domain_instagram_admin_settings_submit';
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}


/**
 * Form validation handler.
 */
function domain_instagram_admin_settings_validate(&$form, &$form_state) {
  // @todo: Do some validation.
}

/**
 * Form submission handler.
 */
function domain_instagram_admin_settings_submit(&$form, &$form_state) {
  global $_domain;
  $domain_id = $_domain['domain_id'];
  $domain_site_name = $_domain['sitename'];  
  if (isset($form_state['values']) && !empty($domain_id)) {
    // Strip any spaces that may have resulted from a copy/paste of tokens    
    $data = array(
      'access_token' => trim($form_state['values']['access_token']),
      'count' => $form_state['values']['count'],
      'width' => $form_state['values']['width'],
      'height' => $form_state['values']['height'],
      'img_resolution' => $form_state['values']['img_resolution'],
    );
    variable_set("domain_instagram_admin_settings_{$domain_id}", $data);
    drupal_set_message(t('Domain instagram admin configuration has been saved for  ' . $domain_site_name), 'status');
  }
}
