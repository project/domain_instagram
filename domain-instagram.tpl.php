<?php
/**
 * @file
 * Domain instagram content.
 *
 * Available variables:
 *  content:   The Instagram images.
 *  response:  Instagram API response.
 */

print render($content);
