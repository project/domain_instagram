<?php

/**
 * @file
 * Hooks provided by the Domain Instagram API.
 */

/**
 * Allow modules to alter block settings.
 *
 * @param string $delta
 *   Block delta. Either user block or tag block.
 * @param array $settings
 *   Block settings.
 * @param array $config
 *   Global Instagram config.
 *   @see domain_instagram_admin_settings variable.
 *
 * @see domain_instagram_block_view().
 */
function hook_domain_instagram_settings_alter($delta, &$settings, &$config) {
  if ($delta == 'domain_instagram_tag') {
    $settings['tag'] = 'Instagram';
  }
}
